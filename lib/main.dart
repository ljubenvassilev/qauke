import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List _quakes;

  void fillList() async {
    _quakes = (await getQuakes())['features'];
    setState(() {

    });
  }

  @override
  void initState() {
    fillList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text(
          'Quakes',
        ),
      ),
      body: _quakes != null ? constructColumn(_quakes) : Container(),
    );
  }
}

Widget constructColumn(List _quakes){
  return ListView.builder(
      itemCount: _quakes.length,
      itemBuilder: (BuildContext context, int position) {
        return Container(
          color: Colors.grey.shade200,
          child: Column(
            children: <Widget>[
              Divider(
                height: 3.4,
              ),
              ListTile(
                title: Text(
                  DateFormat('d MMMM, yyyy h:mm a').format(DateTime.fromMicrosecondsSinceEpoch(_quakes[position]['properties']['time'] * 1000).toLocal()),
                  style: TextStyle(
                    color: Colors.orange,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500
                  ),
                ),
                subtitle: Text(
                  _quakes[position]['properties']['place'],
                  style: TextStyle(
                    color: Colors.grey.shade600,
                    fontSize: 16.0,
                    fontStyle: FontStyle.italic
                  ),
                ),
                leading: CircleAvatar(
                  backgroundColor: Colors.green.shade700,
                  radius: 25.0,
                  child: Text(
                    _quakes[position]['properties']['mag'].toStringAsFixed(1),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0
                    ),
                  ),
                ),
                onTap: () => showTapMessage(context, _quakes[position]['properties']),
              ),
            ],
          ),
        );
      }
  );
}

Future<Map> getQuakes() async {
  String apiUrl = 'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson';
  http.Response response = await http.get(apiUrl);
  return json.decode(response.body);
}

void showTapMessage(BuildContext context, dynamic properties){
  showDialog(context: context, builder: (context) {
    return AlertDialog(
      title: Text('Quakes'),
      content: Text('M ${properties['mag'].toString()} - ${properties['place']}'),
      actions: <Widget>[
        FlatButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text('OK'),
        )
      ],
    );
  });
}